﻿$(document).ready(function () {


    // ### Галереи Slick ###

    // Галерея телевизоров
    $(".gallery").slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        infinite: true,
        centerMode: true,
        dots: true,
        arrows: true,
        adaptiveHeight: true,
        lazyLoad: "ondemand",
        responsive: [
            {
                breakpoint: 650,
                settings: {
                    centerMode: false,
                    slidesToShow: 1,
                    arrows: true,
                    dots: false
                }
          }]
    });

    // Галерея сертификатов
    $(".certificate-slider").slick({
        slidesToShow: 3,
        infinite: true,
        prevArrow: "<i class=\"material-icons prev2\">keyboard_arrow_left</i>",
        nextArrow: "<i class=\"material-icons next2\">keyboard_arrow_right</i>",
        arrows: false,
        dots: false,
        responsive: [
            {
                breakpoint: 800,
                settings: {
                    arrows: true,
                    slidesToShow: 2
                }
      }]
    });


    // ### Боковое меню ###

    // Нажатие на кнопку гамбургера (открыть меню)
    $("#nav-btn-open").click(function () {
        $("body").toggleClass("openNav");
    });

    // Нажатие на кнопку крестика (закрыть меню)
    $("#nav-btn-cancel").click(function () {
        $("body").toggleClass("openNav");
    });


    // ### Сохранение введеных данных в LocaleStorage ###

    if (window.localStorage) {

        let elements = document.querySelectorAll("[name]");

        for (var i = 0, length = elements.length; i < length; i++) {
            (function (element) {
                let name = element.getAttribute("name");

                element.value = localStorage.getItem(name) || "";

                element.onkeyup = function () {
                    localStorage.setItem(name, element.value);
                };
            })(elements[i]);
        }
    }

    // ### Работа с кнопками и History API ###

    var showModal = function () {
        $("#modal").toggleClass("dn");
    }

    window.addEventListener("popstate", function (e) {
        showModal();
    });

    let buttons = document.querySelectorAll(".slbutt");


    for (var i = 0; i < buttons.length; i++) {
        buttons[i].addEventListener("click", function (e) {
            var state;
            if (e.target.tagName !== "A") return;
            state = {
                page: e.target.getAttribute("href")
            };
            history.pushState(state, "", state.page);
            showModal();
            e.preventDefault();
        });
    }

    // ### Работа с формой, AJAX запрос серверу с помощью jQuery AJAX ###
    // 1. параметр FormID = "myform",
    // id тэга <form> для нужной нам формы
    // 2. параметр Submit Button ID = "myform-sumbit",
    // id кнопки отправки нужной формы
    // Вызываем AJAX функцию с нашими параметрами формы
    AJAXform("myform", "myform-sumbit");
    AJAXform("modal-myform", "modal-myform-sumbit");

    // Сама функция, которую мы вызываем
    function AJAXform(formID, buttonID) {
        // Находим форму по ID.
        var form = document.getElementById(formID);
        // Находим кнопку формы по ID.
        var formButton = document.getElementById(buttonID);
        // Получаем ссылку на formcarry из action.
        var formAction = form.getAttribute("action");
        // Получаем метод формы (post)
        var formMethod = form.getAttribute("method");
        // Получем input'ы формы.
        var formInputs = form.querySelectorAll("input");
        // Получаем textarea формы.
        var formTextAreas = form.querySelectorAll("textarea");

        // Функция для отправки данных на сервер
        function send() {

            // Создаем объект данных формы
            var formData = new FormData();

            // Заполняем объект данных формы
            for (var i = 0; i < formInputs.length; i++) {
                // Добавляем все инпуты в formData().
                formData.append(formInputs[i].name, formInputs[i].value);
            }
            for (var i = 0; i < formTextAreas.length; i++) {
                // Добавляем все textareas в formData().
                formData.append(formTextAreas[i].name, formTextAreas[i].value);
            }

            // Отправляем на сервер данные
            $.ajax({
                // URL сервера
                url: formAction,
                // Данные для отправки
                data: formData,
                // Метод отправки (post)
                type: formMethod,
                // Тип данных
                dataType: "json",
                // Отключаем тип в виде спец. строки
                contentType: false,
                // Отключаем преобразование данных в спец. строку
                processData: false,
                // Вешаем функцию при успешной отправке
                success: function (data) {
                    console.log("### УСПЕШНО ОТПРАВЛЕНО ###");
                    console.log("Данные: ");
                    console.log(data);
                    // Находим место для заголовка
                    var title = document.getElementById("form-title");
                    // Находим место для сообщения
                    var message = document.getElementById("form-message");
                    // Заменяем текст заголовка на полученный с сервера
                    title.innerHTML = data.title;
                    // Заменяем сообщения заголовка на полученный с сервера
                    message.innerHTML = data.message;
                    
                    // Функция закрытия формы, если она в виде модального окна
                    function closeForm() {
                        if(!$("#modal").hasClass("dn")) history.back();
                    }
                    
                    // Ставим таймер на 2 секунды для закрытия окна
                    // Чтобы пользователь увидел сообщение и форма закрылась
                    setTimeout(closeForm, 2000);
                },
                // Вешаем функцию при ошибке отправки
                error: function(jqXHR, textStatus, errorThrown) {
                    alert("Ошибка отправки! Подробнее в консоли разработчка.");
                    console.log("### ОШИБКА ###");
                    console.log(jqXHR);
                    console.log(textStatus);
                    console.log(errorThrown);
                }

            });

        }

        // При нажатии на кнопку
        formButton.onclick = function () {
            // Вызываем функцию, прописанную выше, для отправки данных на сервер
            send();
            // Очищаем LocaleStorage
            localStorage.clear();
        }

        // Запрещаем обновление страницы
        form.onsubmit = function () {
            return false;
        }
    }
});

// TWFkZSBieSBEbWl0cml5IE1lc2hhcmk=